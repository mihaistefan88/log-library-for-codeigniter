<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Log Class
 *
 * Simple class for advanced logging
 *
 * @package        	CodeIgniter
 * @subpackage    	Libraries
 * @category    	Libraries
 * @author        	Mihai Stefan
 * @license         LGPL v3
 * @link			https://bitbucket.org/mihaistefan88/log-library-for-codeigniter/
 * @version 		0.1
 */
class Log {
	private $actions;
	private $dbtable;
	private $db;
	private $ci;
	private $file;
	private $filename;
	private $logdir;
	private $fh;

	/** 
	 * Constructor function
	 * 	
	 * Function which configures the instance of Log class
	 *
	 * @param array $config array with the config options
	 * @return void
	 *
	 */

	public function __construct($config_new = false){

		//instantiate CI
		$this->ci = &get_instance();

		//default options

		$this->actions = array(
								'login','upload','download','modif_perms','mkdir','mkfile','rmdir','rmfile',
								'copyfile','movefile','adduser','edituser','rmuser','addrole','editrole','rmrole',
								'editsettings','addcompany','editcompany','rmcompany','adddepartament','editdepartament',
								'rmdepartament','activatecompany','deactivatecompany','view'
							);

		$this->dbtable = 'logs';
		$this->db = false;
		$this->file = true;
		$this->filename = 'dms_logs_'.date('Y-m-d').'.log.txt';
		$this->logdir = FCPATH.'logs/';

		//setting options
		if ($config_new) foreach ($config_new as $key=>$value) {
			if ($value != '') 
				$this->{$key} = $value;
		}

		if ($this->file) {
			if (!is_dir($this->logdir)) {
				@mkdir($this->logdir.'/',0777);
			}
			$this->fh = fopen($this->logdir.$this->filename,'a');
		}

	}


	/** 
	 * Message function
	 * 	
	 * Function which inserts the message into the log file or into the database 
	 *
	 * @param array $action action for which the message is inserted
	 * @param array $params parameters to replace in message
	 * @return boolean
	 *
	 */
	public function message($action, $params, $message = false) {
		
		if (!$message)
			$mesaj = vsprintf(lang('log_'.$action),$params);
		else 
			$mesaj = vsprintf($message,$params);
		
		if ($this->file) {
			fwrite($this->fh, date('Y-m-d H:i:s') . ' -> ' . $mesaj . "\r\n");
		}
		if ($this->db) {
			$data['params'] = json_encode($params);
			$data['mesaj'] = $mesaj;
			$data['data'] = date('Y-m-d H:i:s');
			if ($this->ci->session->userdata('user')) $data['id_user'] = $this->ci->session->userdata('user')->id;
			$data['ip'] = $_SERVER['REMOTE_ADDR'];
			$this->ci->db->insert($this->dbtable, $data);
			return true;
		}
		return false;
	}

}